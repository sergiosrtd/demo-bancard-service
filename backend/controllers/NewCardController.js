import database from "../database/database.js";
import {faker} from "@faker-js/faker";
import Bancard from "../services/Bancard.js"
export default (request,response)=>{
    let user = {
        name:faker.name.firstName(),
        phone:faker.phone.phoneNumber(),
        email:faker.internet.email()
    }

    database.insert('users',user)
            .on('result',result=>{
                let inserted_user = {
                    id:result.insertId,
                    ...user
                }
                database.insert('cards',{})
                        .on('result',result=>{
                            let inserted_card = {
                                id:result.insertId
                            }
                            Bancard.createCard({user:inserted_user,card:inserted_card})
                                   .then((bancard_response)=>{
                                         console.log(bancard_response)
                                        response.json({
                                            user:inserted_user,
                                            card:inserted_card,
                                            process_id:bancard_response.data.process_id
                                        });
                                   })
                                   .catch(error=>{
                                    console.log(error)
                                   })
                            
                        })
            })
}