import express from "express"
import cors from "cors"
const app = express()
const port = 3000
app.use(cors())
app.listen(port)


import NewCardController from './controllers/NewCardController.js'


app.get('/new-card', NewCardController)

